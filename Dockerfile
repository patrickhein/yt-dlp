FROM alpine:latest

ADD https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp /usr/local/bin/
RUN apk add --no-cache ca-certificates ffmpeg python3 && chmod 755 /usr/local/bin/yt-dlp

VOLUME /data
WORKDIR /data

USER nobody
ENTRYPOINT ["yt-dlp"]
